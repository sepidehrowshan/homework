package nuvalence;

public class MyException extends RuntimeException {
    public MyException(String message) {
        super("\n****** SORRY NOT POSSIBLE TO CONTINUE: " + message);
    }

    public MyException(Throwable cause) {
        super(cause);
    }
}
