package nuvalence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import nuvalence.MyException;

@EqualsAndHashCode
@Setter
@Getter
public class Edge {
    private Node startingNode;
    private Node endingNode;


    public Edge(Node startingNode, Node endingNode) {
        if (startingNode.equals(endingNode)) {
            throw new MyException("Not a line when 2 nodes overlapping!!! "
                    + startingNode + " and " + endingNode
            );
        } else {
            this.startingNode = startingNode;
            this.endingNode = endingNode;
        }
    }

    public Node getStartingNode() {
        return startingNode;
    }

    public Node getEndingNode() {
        return endingNode;
    }


    public double getLen() {
        if (startingNode.getX() == endingNode.getX()) {
            return Math.abs(endingNode.getY() - startingNode.getY());
        } else if (startingNode.getY() == endingNode.getY()) {
            return Math.abs(endingNode.getX() - startingNode.getX());
        } else {
            //todo: for polygons which are not rectangle
            System.out.println("length calculation not implemented");
            return -1;
        }
    }

    @Override
    public String toString() {
        return "Edge from " +
                "(" + startingNode.getX() + ',' + startingNode.getY() + ")" +
                " to  " +
                "(" + endingNode.getX() + ',' + endingNode.getY() + ")" ;
    }
}
