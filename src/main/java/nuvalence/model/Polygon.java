package nuvalence.model;

import nuvalence.MyException;

import java.util.List;

public class Polygon {
    private List<Node> nodes;
    Polygon(List<Node> nodes) {
        if (nodes.size() < 3) {
            throw new MyException("Not enough nodes to make a polygon");
        } else {
            this.nodes = nodes;
        }
    }
}

