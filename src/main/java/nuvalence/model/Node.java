package nuvalence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@EqualsAndHashCode
@ToString
public class Node {
    private final double x;
    private final double y;

    public Node(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
