package nuvalence.model;

public enum Type {
    COLLINEAR,
    PERPENDICULAR,
    PARALLEL,
    LAYOVER
}
