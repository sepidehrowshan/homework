package nuvalence.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import nuvalence.MyException;

import java.util.List;
import java.util.stream.Collectors;

@Setter
@Getter
@EqualsAndHashCode
public class Rectangle extends Polygon{
    Node a;
    Node b;
    Node c;
    Node d;
    Edge ab;
    Edge bc;
    Edge cd;
    Edge da;
    List<Node> ns;
    List<Edge> es;


    public Rectangle(List<Node> nodes) {
        super(nodes);
        // checking the number of nodes/edges
        if (nodes.size() != 4) {
            throw new MyException("Not enough nodes to be a RECTANGLE");
        }

        // TODO add validation aside of node numbers
        int i = 0;

        this.ns = nodes.stream().collect(Collectors.toList());

        this.a = nodes.get(0);
        this.b = nodes.get(1);
        this.c = nodes.get(2);
        this.d = nodes.get(3);

        this.ab = new Edge(nodes.get(0), nodes.get(1));
        this.bc = new Edge(nodes.get(1), nodes.get(2));
        this.cd = new Edge(nodes.get(2), nodes.get(3));
        this.da = new Edge(nodes.get(3), nodes.get(0));

        this.es = List.of(ab, bc, cd, da);

    }

    // (Y2 - Y1)/(X2 - X1)
    // however considering our case of rectangles
    // vertical = undefined so we return max
    // horizontal = 0
    private double getSlop(Node s, Node e) {
        if (e.getX() - s.getX() != 0) {
            return 0;
            //return (e.getY() - s.getY())/(e.getX() - s.getX());
        }
        return Integer.MAX_VALUE;
    }

}
