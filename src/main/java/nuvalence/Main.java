package nuvalence;

import nuvalence.model.Rectangle;

import java.util.Scanner;

import static java.lang.System.exit;
import static nuvalence.Util.*;


public class Main {
    static boolean a_contain_b = false;
    static boolean b_contain_a = false;
    static boolean a_intersect_b = false;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your first rectangle nodes in order and format x,y;xx,yy;xxx,yyy;xxxx,yyyy: ");

        // get their input as a String
        String first = scanner.next();
        Rectangle recA = getRectangleFromCommandLine(first.split(";"));
        // prompt for their age
        System.out.print("Now enter your second rectangle please:");

        // get the age as an int
        String second = scanner.next();
        Rectangle recB = getRectangleFromCommandLine(second.split(";"));
        if (recA.equals(recB)) {
            System.out.println("totally the same, not going to look for anything else");
            exit(0);
        }

        System.out.println("Check if First is containing Second rectangle:");
        a_contain_b = checkContainment(recA, recB);
        System.out.println(a_contain_b);

        System.out.println("Check if Second is containing First rectangle:");
        b_contain_a = checkContainment(recB, recA);
        System.out.println(b_contain_a);


        System.out.println("Check if there is any Intersection or Adjacency:");
        a_intersect_b = checkIntersect(recA, recB);

    }
}
