package nuvalence;


import nuvalence.model.Edge;
import nuvalence.model.Node;
import nuvalence.model.Rectangle;
import nuvalence.model.Type;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static nuvalence.model.Type.*;

public class Util {
    static int INF = 10000;

    /**
     * reading args into rectangle nodes
     * @param args
     * @return
     */
    public static Rectangle getRectangleFromCommandLine(String[] args) {
        List<Node> nodes = new ArrayList<>();

        for (String str : args) {
            var tmp = Arrays.stream(str.split(","))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            nodes.add(new Node(tmp.get(0), tmp.get(1)));
        }
        //System.out.println(nodes.size());
        return new Rectangle(nodes);
    }

    /**
     * check is first rectangle contain the second one
     * @param a Rectangle
     * @param b Rectangle
     * @return
     */
    public static boolean checkContainment(Rectangle a, Rectangle b) {
        int insideNodesCount = 0;
        for (Node n :b.getNs()) {
            if (isInside(a.getNs(), n)) insideNodesCount++;
        }
        return insideNodesCount == 4;
    }

    public static boolean checkIntersect(Rectangle recA, Rectangle recB) {
        boolean intersectFlag = false;
        List<Node> intersectingNodes = new ArrayList<>();
        for (Edge eA: recA.getEs()) {
            for (Edge eB: recB.getEs()) {
                if (doIntersect(eA, eB) == PERPENDICULAR) {
                    //todo better logic as a seprate method
                    if (eA.getStartingNode().getX() - eA.getEndingNode().getX() == 0) {
                        intersectingNodes.add(new Node(eA.getStartingNode().getX(), eB.getStartingNode().getY()));
                    } else {
                        intersectingNodes.add(new Node(eB.getStartingNode().getX(), eA.getStartingNode().getY()));
                    }
                    intersectFlag = true;
                } else if (doIntersect(eA, eB) == COLLINEAR) {
                    double eA_len = eA.getLen();
                    double eB_len = eB.getLen();
                    int diff = Long.signum((long)eA_len - (long)eB_len);
                    if (diff != 0) {
                        System.out.println("Sub-Line Adjacent");
                    } else {
                        // they can be same len but different x or y, so have to check for partial
                        if (
                                eA.getStartingNode().getX() == eB.getStartingNode().getX() ||
                                        eA.getStartingNode().getX() == eB.getEndingNode().getX() ||
                                        eA.getStartingNode().getY() == eB.getStartingNode().getY() ||
                                        eA.getStartingNode().getX() == eB.getEndingNode().getX()

                        )
                            System.out.println("Proper Adjacent");
                        else
                            System.out.println("Partial Adjacent");
                    }
                    return true;
                }
            }
        }
        if (intersectingNodes.size() > 0) {
            System.out.println("lets print out coordinates");
            for (Node n : intersectingNodes){
                System.out.println(n.toString());
            }
            return true;
        }
        return intersectFlag;
    }


    // Given three collinear points p, q, r, the function checks if
    // point q lies online segment 'pr'
    static boolean onSegment(Node p, Node q, Node r)
    {
        if (q.getX() <= Math.max(p.getX(), r.getX()) && q.getX() >= Math.min(p.getX(), r.getX()) &&
                q.getY() <= Math.max(p.getY(), r.getY()) && q.getY() >= Math.min(p.getY(), r.getY()))
            return true;
        return false;
    }

    // To find orientation of ordered triplet
    // (p1, p2, p3). The function returns
    // following values
    // 0 --> p, q and r are collinear
    // 1 --> Clockwise
    // 2 --> Counterclockwise
    public static int orientationByNode(Node p1, Node p2, Node p3)
    {
        //https://www.geeksforgeeks.org/orientation-3-ordered-points/amp/
        double val = (p2.getY() - p1.getY()) * (p3.getX() - p2.getX()) -
                (p2.getX() - p1.getX()) * (p3.getY() - p2.getY());
        if (val == 0) return 0;  // collinear
        // clock or counterclockwise
        return (val > 0)? 1: 2;
    }

    // The main function that returns true if line segment 'p1q1'
    // and 'p2q2' intersect.
    static Type doIntersect(Edge a, Edge b) {
        // Find the four orientations needed for general and
        // special cases
        int o1 = orientationByNode(a.getStartingNode(), a.getEndingNode(), b.getStartingNode());
        int o2 = orientationByNode(a.getStartingNode(), a.getEndingNode(), b.getEndingNode());
        int o3 = orientationByNode(b.getStartingNode(), b.getEndingNode(), a.getStartingNode());
        int o4 = orientationByNode(b.getStartingNode(), b.getEndingNode(), a.getEndingNode());

        // General case
        if (o1 != o2 && o3 != o4)
            return PERPENDICULAR;

        // Special Cases
        // p1, q1 and p2 are collinear and p2 lies on segment p1q1
       if (o1 == 0 && onSegment(a.getStartingNode(), b.getStartingNode(), a.getEndingNode())) return COLLINEAR;

        // p1, q1 and q2 are collinear and q2 lies on segment p1q1
        if (o2 == 0 && onSegment(a.getStartingNode(), b.getEndingNode(), a.getEndingNode())) return COLLINEAR;

        // p2, q2 and p1 are collinear and p1 lies on segment p2q2
        if (o3 == 0 && onSegment(b.getStartingNode(), a.getStartingNode(), b.getEndingNode())) return LAYOVER;

        // p2, q2 and q1 are collinear and q1 lies on segment p2q2
        if (o4 == 0 && onSegment(b.getStartingNode(), a.getEndingNode(), b.getEndingNode())) return LAYOVER;

        return PARALLEL; // Doesn't fall in any of the above cases
    }


    // Returns true if the point p lies
    // inside the polygon[] with n vertices
    static boolean isInside(List<Node> polygonNodes, Node theNode)
    {
        int n = 4;
        // Create a point for line segment from theNode to infinite
        Node extreme = new Node(INF, theNode.getY());

        // Count intersections of the above line
        // with sides of polygon
        int count = 0, i = 0;
        do {
            int next = (i + 1) % n;
            Edge givenEdge = new Edge(polygonNodes.get(i), polygonNodes.get(next));

            // Check if the line segment from 'theNode' to
            // 'extreme' intersects with the line
            // segment from 'polygon[i]' to 'polygon[next]'
            if (doIntersect(givenEdge, new Edge(theNode,extreme)) != PARALLEL)
            {
                // If the point 'p' is collinear with line
                // segment 'i-next', then check if it lies
                // on segment. If it lies, return true, otherwise false
                if (orientationByNode(polygonNodes.get(i), theNode, polygonNodes.get(next)) == 0)
                {
                    return onSegment(polygonNodes.get(i), theNode,
                            polygonNodes.get(next));
                }

                count++;
            }
            i = next;
        } while (i != 0);

        // Return true if count is odd, false otherwise
        return (count % 2 == 1); // Same as (count%2 == 1)
    }
}
