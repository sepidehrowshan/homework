One of approaches for working with Polygons is using the java.awt libs; 
However, the implementation here is by POJO, creating a polygon and extending rectangle

### Definition of a Polygon used in here
A closed shape with n >= 3 nodes

### Rectangle definition with assumptions based on homework
Axes are shared between testing rectangles
The value of x or y of a node can be negative
The edges' lengths or distance between objects is less than 1000
The input for nodes is sequential but does not matter clockwise or counterclockwise


for each node in list except last one, create the edge between node and node + 1

## How to run
mvn exec:java -Dexec.mainClass=nuvalence.Mai


#### references used:
https://www.geeksforgeeks.org/how-to-check-if-a-given-point-lies-inside-a-polygon/
https://www.geeksforgeeks.org/check-if-two-given-line-segments-intersect/amp/
